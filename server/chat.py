import numpy as np
import pandas as pd
import csv
import re
import urllib.request
import json

lind = "mustluik"
lind = lind.lower();
linnud = pd.read_csv('Eesti_linnud.csv', sep=";", encoding='utf-8')

kysimus = "Kuidas on      Bufflehead eesti keeles"
kysimus = kysimus.lower()
kysimus = re.sub('\s+'," ", kysimus)
print(kysimus)
for col in linnud.columns:
    if isinstance(linnud[col][0], str):
        linnud[col] = linnud[col].str.strip()
        linnud[col] = linnud[col].str.lower()
    else:
        data[col] = linnud[col].replace(" ","")

#print(linnud.nimi_ek)
#print(linnud[(linnud['nimi_ek' == lind]) | (linnud['nimi_ik' == lind]) | (linnud['nimi_lk' == lind])])
#print(linnud[linnud.nimi_ek.str.contains(lind)==True].nimi_lk)

# python oli ESTNLTK jaoks liiga uus. Enam ei saanud seda muuta.
if "eesti keel" in kysimus:
    sonad = kysimus.split(" ")
    vasted = []
    for sona in sonad:
        if len(linnud[linnud.nimi_ik == sona].nimi_ek) > 0:
            vasted += linnud[linnud.nimi_ik == sona].nimi_ek.tolist()
        if len(linnud[linnud.nimi_ik == sona].nimi_ek) > 0:
            vasted += linnud[linnud.nimi_lk == sona].nimi_ek.tolist()

print(vasted)
