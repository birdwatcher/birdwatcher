# birdwatcher

Rakendusel on back-end, mis on tehtud Flaskiga ja front-end, mis on tehtud Vue.js-iga.

Rakendust on lokaalses arvutis kõige kergem käivitada PyCharmis, millesse on lisatud Vue.js plugin. 
Back-end asub kaustas server ja front-end kaustas client. 

Käivitamiseks:
1. Käivita back-end kaustas birdwatcher/server - run app.py (brauseris - localhost:5000)
2. Enne front-endi käivitamist jooksuta olles kaustas  birdwatcher/client käsku npm install (seda ei pea alati tegema, ainult uuenduste allalaadimisel)
3. Olles kaustas birdwatcher/client anna terminali aknas käsk - npm run serve (brauseris - localhost:8080) 
